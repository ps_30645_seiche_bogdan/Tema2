using Microsoft.EntityFrameworkCore;
using BeautySalon.Models;

namespace BeautySalon.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }

        public DbSet<BeautySalon.Models.Appointment> Appointment { get; set; }
    }
}