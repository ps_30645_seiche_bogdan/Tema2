using BeautySalon.FileExport;
using BeautySalon.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace BeautySalonTest
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void JsonExportTest()
        {
            List<Appointment> list = new List<Appointment>()
            {
                new Appointment(0, "Ion", new DateTime(2021,6,24,12,0,0), "0740123456", ServiceType.FACE),
                new Appointment(0, "Andreea", new DateTime(2021,6,29,8,0,0), "0740123222", ServiceType.MASSAGE),
                new Appointment(0, "George", new DateTime(2021,7,1,12,0,0), "0740331456", ServiceType.NAILS)
            };

            var stream = new ExporterFactory().GetExporter(ExporterFactory.ExportType.JSON).Export(list);

            Assert.That(stream.ToString(), Does.Contain("Ion").IgnoreCase);
        }
    }
}