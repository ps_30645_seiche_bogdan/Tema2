﻿using BeautySalon.Models;
using System.Collections.Generic;

namespace DAL.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        List<TEntity> GetAll();
        void Insert(TEntity entity);
        void Delete(TEntity entity);
        TEntity GetByID(object id);
        void Update(TEntity entity);
    }
}