﻿using BeautySalon.Data;
using BeautySalon.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalon.Dal
{
    public class UnitOfWork
    {
        private DatabaseContext _context;
        private IRepository<User> userRepository;
        private IRepository<Appointment> appointmentRepository;
        private bool disposed = false;

        public UnitOfWork()
        {
            _context = new DatabaseContext();
        }

        public IRepository<User> UsersRepository
        {
            get
            {

                if (this.userRepository == null)
                {
                    this.userRepository = new GenericRepository<User>(_context);
                }
                return userRepository;
            }
        }

        public IRepository<Appointment> AppointmentsRepository
        {
            get
            {

                if (this.appointmentRepository == null)
                {
                    this.appointmentRepository = new GenericRepository<Appointment>(_context);
                }
                return appointmentRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
