using System;
using System.ComponentModel.DataAnnotations;

namespace BeautySalon.Models
{
    public enum UserType
    {
        EMPLOYEE,
        ADMIN,
        MANGER
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public UserType  UserType { get; set; }
    }
}