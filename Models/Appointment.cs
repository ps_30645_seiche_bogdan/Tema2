using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace BeautySalon.Models
{
    public enum ServiceType
    {
        NAILS,
        HAIR,
        FACE,
        MASSAGE
    }

    public class Appointment
    {
        public Appointment(int id, string clientName, DateTime time, string clientPhone, ServiceType service)
        {
            Id = id;
            ClientName = clientName;
            Time = time;
            ClientPhone = clientPhone;
            Service = service;
        }

        public int Id { get; set; }
        public string ClientName { get; set; }

        public DateTime Time { get; set; }
        public string ClientPhone { get; set; }

        public ServiceType  Service { get; set; }
    }
}