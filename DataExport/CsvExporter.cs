﻿using BeautySalon.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalon.FileExport
{
    public class CsvExporter : Exporter
    {
        public MemoryStream Export(List<Appointment> appointments)
        {
            string data = String.Join(",", appointments.Select(x => x.ToString()).ToArray());

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);

            writer.Write(data);
            writer.Flush();
            stream.Position = 0;

            return stream;
        }
    }
}
