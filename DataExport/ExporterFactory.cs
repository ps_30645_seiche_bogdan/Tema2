﻿using BeautySalon.DataExport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeautySalon.FileExport
{
    public class ExporterFactory
    {
        public enum ExportType
        {
            CSV,
            JSON,
            PDF
        }

        public Exporter GetExporter(ExportType exportType)
        {
            switch (exportType)
            {
                case ExportType.JSON:
                    return new JsonExporter();
                case ExportType.CSV:
                    return new CsvExporter();
                case ExportType.PDF:
                    return new PdfExporter();
                default:
                    return null;
            }
        }
    }
}
