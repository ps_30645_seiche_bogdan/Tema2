﻿using BeautySalon.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BeautySalon.FileExport
{
    public interface Exporter
    {
        public MemoryStream Export(List<Appointment> appointments);
    }
}
