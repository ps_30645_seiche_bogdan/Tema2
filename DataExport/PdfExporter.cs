﻿using BeautySalon.FileExport;
using BeautySalon.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace BeautySalon.DataExport
{
    public class PdfExporter : Exporter
    {
        public MemoryStream Export(List<Appointment> appointments)
        {
            string data = String.Join("\n", appointments.Select(x => x.ToString()).ToArray());


            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            pdfDoc.Open();

            Chunk chunk = new Chunk(data);
            pdfDoc.Add(chunk);
            pdfDoc.Close();


            var stream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
            stream.Position = 0;

            return stream;
        }
    }

}