﻿using BeautySalon.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BeautySalon.FileExport
{
    public class JsonExporter : Exporter
    {
        public MemoryStream Export(List<Appointment> appointments)
        {
            string data =  JsonSerializer.Serialize(appointments);
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);

            writer.Write(data);
            writer.Flush();
            stream.Position = 0;

            return stream;
            return stream;
        }
    }
}
