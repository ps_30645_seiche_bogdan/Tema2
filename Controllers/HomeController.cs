﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BeautySalon.Models;
using Microsoft.AspNetCore.Http;
using BeautySalon.Dal;

namespace BeautySalon.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private UnitOfWork unitOfWork = new UnitOfWork();


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public ActionResult Login(string userName, String password)
        {
            User user = unitOfWork.UsersRepository.GetByID(userName);
            if (user != null && user.Password == password)
            {
                HttpContext.Session.SetString("user_name", user.Username);
                HttpContext.Session.SetString("user_role", user.UserType.ToString());
                return RedirectToAction("Index", "AppointmentController", new { area = "" });
            }
            return View();

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
