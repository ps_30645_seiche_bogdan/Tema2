using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeautySalon.Data;
using BeautySalon.Models;
using BeautySalon.Dal;

namespace BeautySalon.Controllers
{
    public class UserController : Controller
    {
        private readonly DatabaseContext _context;
        private UnitOfWork unitOfWork = new UnitOfWork();


        public UserController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Login
        public ViewResult Index()
        {
            return View(unitOfWork.UsersRepository.GetAll().ToList());
        }

        // GET: Login/Details/5
        public ViewResult Details(int? id)
        {
            User user = unitOfWork.UsersRepository.GetByID(id);

            return View(user);
        }

        // GET: Login/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Id,Name,Username,Password,UserType")] User user)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.UsersRepository.Insert(user);
                unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Login/Edit/5
        public ActionResult Edit(int? id)
        {
            User user = unitOfWork.UsersRepository.GetByID(id);
            return View(user);
        }

        // POST: Login/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind("Id,Name,Username,Password,UserType")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    unitOfWork.UsersRepository.Update(user);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Login/Delete/5
        public ActionResult Delete(int? id)
        {
            User user = unitOfWork.UsersRepository.GetByID(id);
            return View(user);
        }

        // POST: Login/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = unitOfWork.UsersRepository.GetByID(id);
            unitOfWork.UsersRepository.Delete(user);
            unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.Id == id);
        }
    }
}
