﻿using BeautySalon.Data;
using BeautySalon.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeautySalon.Controllers
{
    public class ApiController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ApiController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet("api/appointments/?{start}&{end}")]
        public ActionResult<IEnumerable<Appointment>> GetByTime(DateTime start, DateTime end)
        {
            List<Appointment> allAppos = _context.Appointment.ToList();
            List<Appointment> matching = new List<Appointment>();
            for (int i = 0; i < allAppos.Count; i++)
            {
                if (allAppos[i].Time > start && allAppos[i].Time < end)
                {
                    matching.Add(allAppos[i]);
                }
            }

            return matching;
        }
    }
}
